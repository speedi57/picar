#!/usr/bin/python
# -*- encoding: utf-8 -*-

# récepteur ovladani TEA5767 FM via la communication I2C
# dernier changement 9.6.2013


import smbus               # fonctionnement avec I2C
import time                # opérations avec le temps (pause)
import sys                 # ENQUÊTE paramètre de ligne de commande 

class tea:
	
	bus = smbus.SMBus(1)       # novejsi varianta RasPi (512MB)
	#bus = smbus.SMBus(0)       # starsi varianta RasPi (256MB)


	# ================================================
	# sous-programme pour Réglage de la fréquence requise
	def nastav_f(self, freq):

	  if (freq >= 87.5 and freq <= 108):  # kdyz je frekvence v pripustnych mezich, zapise se do obvodu

		# Le taux de conversion à deux octets (selon directives d'application)
		freq14bit = int(4 * (freq * 1000000 + 225000)/32768)
		freqH = int(freq14bit / 256 )
		freqL = freq14bit & 0xFF

									 # Description de chaque bit dans un octet - voir. Fiche produit
		bajt0 = 0x60                 # Adresse circuit I2C
		bajt1 = freqH                # 1.bajt (bit MUTE, la fréquence H)
		bajt2 = freqL                # 2.bajt (fréquence basse)
		bajt3 = 0b10110000           # 3.bajt (SUD  ;  SSL1,SSL2  ;  HLSI  ;  MS,MR,ML  ;  SWP1)
		bajt4 = 0b00010000           # 4.bajt (SWP2 ; STBY ;  BL ; XTAL ; SMUTE ; HCC ; SNC ; SI)
		bajt5 = 0b00000000           # 5.bajt (PLREFF  ;  DTC  ;  0;0;0;0;0;0)

		blokdat=[ bajt2 , bajt3 , bajt4 , bajt5 ]
		bus.write_i2c_block_data( bajt0 , bajt1 , blokdat )   # Mise en place d'une nouvelle fréquence dans le circuit

		time.sleep(0.1)                 # attendre signalent évalue la force
		bajt1r = bus.read_byte(bajt0)   # lire le premier octet (à lire indépendamment)
		data = bus.read_i2c_block_data( bajt0 , bajt1 )     # lire des données provenant du circuit de vérifier le niveau des signaux 
		data[0] = bajt1r                # Tous les non lus remplacer le premier octet chargé séparément valeur
		print "frekvence= " + str(freq) + "MHz" , "\tDATA: " + str(data[0:5]) + "\t(Sila signalu:" + str(data[3]>>4) + ")"

	  return(freq)

	# ================================================
	# sous-programme pour la recherche de la station la plus proche en saisissant la fréquence
	def sken(self ,freq , smer):

	  if (sys.argv[1] == "-v"):             # Quand il énumère toutes les hautes fréquences ...
		jen_1_freq = False                  #     Quoiqu'il en soit, la seule fréquence
		try:
		  adc_limit = int(sys.argv[2])      # vérifier le niveau minimum paramètre de convertisseur AD pour autosken
		except:
		  adc_limit=7                       # Si l'erreur, il sera automatiquement 7
	  elif (sys.argv[1] == "-t"):           # en numérisation à l'aide des boutons
		jen_1_freq = True                   #    est à la recherche d'une seule fréquence
		try:
		  adc_limit = int(sys.argv[2])      # vérifier le niveau minimum paramètre de convertisseur AD pour autosken
		except:
		  adc_limit=7                       # Si l'erreur, il sera automatiquement 7

	  else:                                 # Lorsque le réglage est recherché que la première fréquence fortement ...
		jen_1_freq = True                   #    la recherche d'une seule fréquence
		try:
		  adc_limit = int(sys.argv[3])      # vérifier le niveau minimum paramètre de convertisseur AD pour autosken
		except:
		  adc_limit=7                       # , l'erreur est automatiquement réglé sur 7
	  
	  
	  if (adc_limit > 15 or adc_limit < 0): #évaluer, si la limite est spécifiée entre 0 et 15
		adc_limit = 7                       # Lorsque l'extérieur, est automatiquement réglé sur 7 

	  if (sys.argv[1] == "-v"):             # fonction de recherche automatique dans toute la bande imprimera infos
		print "Affichage des fréquences avec un signal minimum " + str(adc_limit)


	  # la boucle principale pour vérifier si la fréquence est dans les limites autorisées
	  while (freq >= 87.5 and freq <= 108):  # lorsque la fréquence est hors de portée, l'expiration de la boucle
		if(smer == True):    # selon la direction de balayage est soit ayant reçu ou reçoit 100kHz
		  freq= freq + 0.1
		else:
		  freq= freq - 0.1

		# Le taux de conversion à deux octets (selon directives d'application)
		freq14bit = int(4 * (freq * 1000000 + 225000)/32768)
		freqH = int(freq14bit / 256 )
		freqL = freq14bit & 0xFF

		mutebit = 0b00000000         # Lors de la recherche pour couper le son (hum / sumi / sable comme une radio normale lors du débogage)
	#   mutebit = 0b10000000        # Lors de la recherche hors du volume

									 # popisy jednotlivych bitu v bajtech - viz. katalogovy list
		bajt0 = 0x60                 # I2C adresa obvodu
		bajt1 = freqH | mutebit      # 1.bajt (MUTE bit ; frekvence H)
		bajt2 = freqL                # 2.bajt (frekvence L)
		bajt3 = 0b10110000           # 3.bajt (SUD  ;  SSL1,SSL2  ;  HLSI  ;  MS,MR,ML  ;  SWP1)
		bajt4 = 0b00010000           # 4.bajt (SWP2 ; STBY ;  BL ; XTAL ; SMUTE ; HCC ; SNC ; SI)
		bajt5 = 0b00000000           # 5.bajt (PLREFF  ;  DTC  ;  0;0;0;0;0;0)

		blokdat=[ bajt2 , bajt3 , bajt4 , bajt5 ]

		# accordé sur la nouvelle fréquence
		bus.write_i2c_block_data( bajt0 , bajt1 , blokdat )

		time.sleep(0.05)  # fréquences respectives attendre signalent évalue la force

		# lire le contenu de tous les registres
		bajt1r = bus.read_byte(bajt0)                       # Le premier octet à lire séparément 
		data = bus.read_i2c_block_data( bajt0 , bajt1 )     # Récupérer tous les octets du circuit
		data[0] = bajt1r                                    # Le premier octet est chargé séparément remplace la valeur

		sila = data[3] >> 4   # Dans les 4 bits supérieurs de la quatrième Baja (données [3]), tandis que la lecture d'un niveau de signal de registre
	 
		if (sila >= adc_limit):   # l'intensité de signal minimum, qui peut être considéré comme une station sélectionnée (0 à 15)
		  print "f= " + str(freq) + "MHz" , "\tDATA:" + str(data[0:5]) + "\t(Sila signalu: " + str(sila) + ")"

		  if (mutebit == 0b10000000):   # l'annulation de tout bit MUTE lorsque la station est trouvé
			bajt1 = bajt1 & 0b01111111
			bus.write_i2c_block_data( bajt0 , bajt1  , blokdat )
		  
		  if (jen_1_freq == True):  # quand consulter un seul de la fréquence la plus proche, ainsi après avoir sorties de la boucle while
			break

	  if (freq > 108):                      # Lorsque vous roulez sur l'extrémité supérieure de la bande ...
		freq=108                            # ... et les rendements finaux ...
		print "atteignant l'extrémité supérieure de la bande"   # et signale que, à la fin
	  if (freq < 87.5):                     # Lorsque vous roulez dans la zone d'extrémité inférieure ...
		freq=87.5                           #Alors ... à la fin de la Porte de ...
		print "atteint l'extrémité inférieure de la bande"  # et signale que, à la fin
	  return (freq)





	# ================================================
	# sous-programme pour couper ou retablir le volume		
	def mute(self,value):
		if (value == True):    # baissez le volume
		  bajt1 = bus.read_byte( 0x60 )
		  bajt1 = bajt1 | 0b10000000 
		  bus.write_byte(0x60,bajt1)



		elif (value == False):    # restaurer le volume
		  bajt1 = bus.read_byte( 0x60 )
		  bajt1 = bajt1 & 0b01111111 
		  bus.write_byte(0x60,bajt1)




# ================================================
# début du programme - Évaluation des paramètres de ligne de commande
# ================================================

if __name__ == '__main__':
	radio = tea()
	try:                        # Si le premier paramètre est manquant, la créé
	  parametr=sys.argv[1]
	except:
	  parametr=""


	if (parametr=="-n"):        # Réglage direct d'une fréquence spécifique
	  try:
		freq= float(sys.argv[2])
	  except:
		freq=0
	  radio.nastav_f(freq)


	elif (parametr == "-sn"):    # Auto Scan 'UP' de la fréquence spécifiée
	  try:
		freq= float(sys.argv[2])
	  except:
		freq=87.5                # Lorsque les données sont manquantes, ou incorrecte, régler la limite inférieure de la
	  radio.sken(freq, True)           # Numérisation à partir d'un UP de fréquence spécifiée



	elif (parametr == "-sd"):    # abalayage automatique "bas" à partir d'une fréquence spécifiée
	  try:
		freq= float(sys.argv[2])
	  except:
		freq=108                 # Lorsque les données sont manquantes, ou incorrecte, régler la limite supérieure de la fourchette
	  radio.sken(freq, False ) # Numérisation à partir d'une fréquence spécifiée BAS



	elif (parametr == "-v"):     # répertorie automatiquement toutes forte fréquence
	  radio.sken(87.5, True )          # numériser à partir du début Bandes UP


	elif (parametr == "-h0"):    # baissez le volume
	  bajt1 = bus.read_byte( 0x60 )
	  bajt1 = bajt1 | 0b10000000 
	  bus.write_byte(0x60,bajt1)



	elif (parametr == "-h1"):    # restaurer le volume
	  bajt1 = bus.read_byte( 0x60 )
	  bajt1 = bajt1 & 0b01111111 
	  bus.write_byte(0x60,bajt1)


	else:                        # Si le premier paramètre ne correspond à aucune variante, l'aide est affichée
	  print "valeurs admissibles:"
	  print "... -n fff.f       réglage de fréquence direct"
	  print "... -sn fff.f LL   trouver la station la plus proche sur une fréquence spécifiée"
	  print "... -sd fff.f LL   trouver la station la plus proche en dessous de la fréquence spécifiée"
	  print "... -v LL          répertorie automatiquement toutes forte fréquence"
	  print "... -h0            coupez le volume (MUTE)"
	  print "... -h1            restaurer le volume"
	  print "... -p             Alternant canaux prédéfinis à l'aide des boutons"
	  print ""
	  print "  fff.f = fréquence en MHz. Les valeurs admises sont comprises entre 87,5 et 108"
	  print "  LL    = l'intensité de signal minimum (ADC_level) à laquelle le signal"
	  print "           recherche automatique considéré pour la station (0 à 15)"
