#!/usr/bin/python
from gi.repository import Gtk as gtk
from gi.repository import  GObject as gobject
import os, configparser, moc,time
from module import player
#from module import tea5767
class interface:
	def __init__(self):
		#self.radio = tea5767()
		self.player = player()
		self.load = False
		self.defaullt = "/data/media/Music"
		self.config = configparser.ConfigParser()
		self.config.read('config.ini')
		interface = gtk.Builder()
		interface.add_from_file('./interface/interface.glade')
		self.volume = interface.get_object("volume")
		self.progression = interface.get_object("progression")
		self.music = interface.get_object("music")
		self.artiste = interface.get_object("artiste")
		self.cover = interface.get_object("pochette")
		self.mode = interface.get_object("mode")
		self.freq = interface.get_object("scale1")
		self.deroul = interface.get_object("liststore1") 
		self.folder = interface.get_object("filechooserbutton1")
		self.tab = interface.get_object('notebook1')
		self.on_notebook1_switch_page(self.tab, None,self.tab.get_current_page())
		#self.fullScreen = interface.get_object("main")
		interface.connect_signals(self)
		gobject.idle_add(self.test)
		print gobject.isVisible()
		if True:#(self.config["player"]["last"] != self.defaullt)
			time.sleep(2)
			self.player.new_folder(self.defaullt)
			self.config["player"]["last"] = self.defaullt
			with open("./config.ini", 'w') as configfile:    # save
				self.config.write(configfile)
		print 'finish'
		#self.fullScreen.resize(1600, 1250)
		
	def on_main_activate_default(self, widget):
		pass
	def test(self):
		music = self.player.getinfo()
		if music != None :
			self.music.set_text(music['songtitle'])
			self.artiste.set_text(music['artist'])	
			self.progression.set_fraction(music['frac'])	
		return 1
	def on_main_destroy(self, widget):
		gtk.main_quit()
		exit()
	def on_test_clicked(self, widget):
		gtk.main_quit()
		exit()
	def on_etteindre_clicked(self, widget):
		gtk.main_quit()
		#~ os.system('sudo halt')
		exit()
	def on_next_clicked(self,widget):
		self.player.next()
	def on_vol_moin_clicked(self,widget):
		self.player.volume_down()
	def on_vol_plus_clicked(self,widget):
		self.player.volume_up()		
	def on_prev_clicked(self,widget):
		self.player.previous()
	def on_aleatoire_clicked(self,widget):
		self.mode.set_text(self.player.random())
	def on_pause_clicked(self,widget):
		self.player.pause()
	def on_play_clicked(self,widget):
		self.player.play()
	def on_stop_clicked(self,widget):
		self.player.stop()
	def on_filechooserbutton1_file_set(self,widget):
		self.player.new_folder(self.folder.get_current_folder())
		self.config["player"]["last"] = self.folder.get_current_folder()
		with open("./config.ini", 'w') as configfile:    # save
			self.config.write(configfile)
	def on_eventbox2_button_press_event(self, widget, event):
		self.xpos = event.x
	def on_eventbox2_button_release_event(self, widget, event):
		self.player.slide(self.xpos,event.x)
	def on_scale1_value_changed(self, widget, event):
		#self.radio.setfreq(self.freq.get_value_pos())
		pass
	def on_button9_clicked(self, widget, event):
		#self.radio.search()
		pass
	def on_stat1_clicked(self, widget, event):
		#self.radio.setfreq(self.config["fm"]["fm1"])
		pass
	def on_stat2_clicked(self, widget, event):
		#self.radio.setfreq(self.config["fm"]["fm2"])
		pass
	def on_stat3_clicked(self, widget, event):
		#self.radio.setfreq(self.config["fm"]["fm3"])
		pass
	def on_stat4_clicked(self, widget, event):
		#self.radio.setfreq(self.config["fm"]["fm4"])
		pass
	def on_stat5_clicked(self, widget, event):
		#self.radio.setfreq(self.config["fm"]["fm5"])
		pass
	def on_stat6_clicked(self, widget, event):
		#self.radio.setfreq(self.config["fm"]["fm6"])
		pass
	def on_stat7_clicked(self, widget, event):
		#self.radio.setfreq(self.config["fm"]["fm7"])
		pass
	def on_stat8_clicked(self, widget, event):
		#self.radio.setfreq(self.config["fm"]["fm8"])
		pass

	def on_notebook1_switch_page(self, widget, label, page):
		if page == 2:
			if self.load == False:
				os.system("pactl load-module module-loopback latency_msec=1")
				self.load = True
			self.player.pause()
			#self.radio.mute(1)
		elif page == 1:
			if self.load == True:
				os.system("pactl unload-module module-loopback")
				self.load = False
			self.player.pause()
			#self.radio.mute(1)
		elif page == 3:
			if self.load == False:
				os.system("pactl load-module module-loopback latency_msec=1")
				self.load = True
			self.player.pause()
			#self.radio.mute(0)
if __name__ == "__main__":
	interface()
	gtk.main()
