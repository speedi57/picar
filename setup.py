#!/usr/bin/python
# -*- coding: utf-8 -*-
 
# source d'inspiration: http://wiki.wxpython.org/cx_freeze
 
import sys, os
from cx_Freeze import setup, Executable
 
#############################################################################
# preparation des options 
path = sys.path
includes = ["module"]
excludes = []
packages = ["sys","moc","subprocess", "gi", "configparser","time"]
includefiles = ["./interface/","config.ini"]


options = {"path": path,
           "includes": includes,
		   "include_files": includefiles,
           "excludes": excludes,
           "packages": packages
           }
 
#############################################################################
# preparation des cibles
base = None
if sys.platform == "win32":
    base = "Win32GUI"
 
cible_1 = Executable(
    script = "main.py",
    base=base,
    compress=True,
    icon=None,
    )
 
#############################################################################
# creation du setup
setup(
    name="PiCar",
    version="0.1",
    description="PiCar PyGTK",
    author="speedi57",
    options={"build_exe": options},
    executables=[cible_1]
    )
