Pi In Car
===================


Pi in Car est un projet visant a utiliser un raspberry en tant qu'autoradio linux.

> Matieriel nescessaire:
> - Raspberry pi 2
> - un ecran tactile : http://www.pollin.de/shop/dt/NTMwOTc4OTk-/Bauelemente_Bauteile/Aktive_Bauelemente/Displays/7_17_78_cm_Display_Set_mit_Touchscreen_LS_7T_HDMI_DVI_VGA_CVBS.html
> - un convertisseur dc 12v->5v http://www.dx.com/p/adjustable-dc-12v-to-dc-5v-voltage-step-down-power-module-black-156743
> - un amplificateur de puissance : http://www.dx.com/p/kentiger-tz-2002-2-channel-hi-fi-car-stereo-audio-amplifier-w-mp3-dvd-vcd-output-red-silver-300958#.VXglqUYmTIU
> - un vielle autoradio a eventrer :P
> - une carte SD d'au moins 4 go de préférence de classe 10

**1) installer de raspbian**

La première étape va être de télécharger raspbian et décompresser l'image:

> en torrent (http://downloads.raspberrypi.org/raspbian_latest.torrent)

> en direct (http://downloads.raspberrypi.org/raspbian_latest)

Passons maintenant à la partie qui nous intéresse à savoir l'installation:
Si vos êtes sous Linux la première étape va être de connaître l'emplacement de votre carte mémoire avec gparted par exemple, dans mon cas ça va être /dev/sdc , pour lancé l'installation ouvrer le terminal et exécuter :

dd if="/emplacement/de/votre/image/" of=/dev/sdc ( emplacer de votre carte sd)

Sous windows :

ouvrir win32 disk imager:

Il faut indiquer l'emplacement du fichier image et la lettre de la carte SD et de cliquer sur Write, il n'y a pu qu'à attendre la fin de l'écriture.

Une fois, terminer on va pouvoir remettre la carte SD dans le raspberry pi et le faire démarrer ( de préférence avec une connexion réseaux).

Personnellement je me connecte au raspberry grâce à kitty qui est un logiciel basé sur putty, dont les login de base de raspbian son pi et rapsberry:


Pour commencer la configuration, on va exécuté :


sudo raspi-config

Puis on va commencé par expand filesystem qui va permettre d'utiliser tout l'espace de stockage de la carte du raspberry pi car oui au départ il n'utilise que 4 go de votre carte mémoire.
La deuxième option va permettre de changer le mot de passe ( important si vous le connecté a internet)
la troisième va servir a choisir le type de démarrage ( mode console, avec interface graphique et autologin et finalement scratch)
la quatrième va servir a choisir la langue du raspberry pi et le format du clavier.
la suivante va permettre d'activer la caméra ( ne l'activer que si vous vous en servez ça consomme pas mal de ressource)
Une fois fini on va pouvoir valider en allant sur finish et redémarrer le raspberry pi.
Maintenant qu'il a fini de redémarrer, on va pouvoir le mettre à jour:
sudo apt-get update 
sudo apt-get upgrade 

**2) installation de l'ecran tactile :**

L’écran se branche directement sur le port HDMI sans trop de problèmes.
Il faut juste modifier le config.txt pour mettre la bonne résolution dans mon cas 1600x600.
J'ai du rajouter les éléments suivants au fichier config qui se situe dans la partition boot :

> #uncomment if you get no picture on HDMI for a default "safe" mode

> #hdmi_safe=1

> #uncomment this if your display has a black border of unused pixels visible

> #and your display can output without overscan

> #disable_overscan=1

> #uncomment the following to adjust overscan. Use positive numbers if console

> #goes off screen, and negative if there is too much border

> #overscan_left=16

> #overscan_right=16

> #overscan_top=16

> #overscan_bottom=16

> #uncomment to force a console size. By default it will be display's size minus

> #overscan. framebuffer_width=1600 framebuffer_height=1200

> #uncomment if hdmi display is not detected and composite is being output

> #hdmi_force_hotplug=1

> #uncomment to force a specific HDMI mode (this will force VGA) hdmi_group=2

> #hdmi_mode=1 HDMI_DMT_UXGA_60 = 0x33

> #uncomment to force a HDMI mode rather than DVI. This can make audio work in

> #DMT (computer monitor) modes

> #hdmi_drive=2

> #uncomment to increase signal to HDMI, if you have interference, blanking, or

> #no display

> #config_hdmi_boost=4

> #uncomment for composite PAL

> #sdtv_mode=2

> #uncomment to overclock the arm. 700 MHz is the default.

> #arm_freq=800

> #for more options see http://elinux.org/RPi_config.txt

Ensuite il faut installer la partie tactile, pour cela il faut installer un module. 
Pour se faire vous avez deux possibilités soit télécharger un kernel tout fais sois le compiler vous même :

- pour en télécharger un tout fait

- pour le compiler vous même (http://engineering-diy.blogspot.com/2013/01/adding-7inch-display-with-touchscreen.html)